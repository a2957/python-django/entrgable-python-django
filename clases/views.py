from django.shortcuts import render

# Create your views here.


from clases.models import Clase
from django.views import View

class ClasesView(View):
    http_method_names = ['post', 'get']

    def get(self, request):
        clases = Clase.objects.all()

        contexto = {
            'clases': clases
        }

        return render(request, 'clases/clases_lista.html', contexto)

    def post(self, request):
        Clase.objects.create(
            nombre  =request.POST['nombre'],
            codigo = request.POST['codigo']
        )
        return self.get(request)

    
class ClaseView(View):
    http_method_names = ['get']
    
    def get(self, request, id):
        clase = Clase.objects.get(id=id)
        estudiantes = clase.estudiantes.all()

        contexto = {
            'clase': clase,
            'estudiantes': estudiantes
        }

        return render(request, 'clases/clase.html', contexto)