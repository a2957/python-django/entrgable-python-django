from django.db import models

# Create your models here.


class Clase(models.Model):
    nombre = models.CharField(max_length=120)
    codigo = models.CharField(max_length=10)
    fecha_creacion = models.DateField(auto_now_add=True)

    def __str__(self) -> str:
        return self.codigo