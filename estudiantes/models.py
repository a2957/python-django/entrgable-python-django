from django.db import models

from clases.models import Clase
# Create your models here.

class Estudiante(models.Model):
    nombre = models.CharField(max_length=120)
    apellido = models.CharField(max_length=120)
    correo = models.CharField(max_length=250)
    clases = models.ManyToManyField(Clase, related_name='estudiantes', blank=True)
    fecha_ingreso = models.DateField(auto_now_add=True)

    def __str__(self) -> str:
        return f'{self.nombre} {self.apellido}'
