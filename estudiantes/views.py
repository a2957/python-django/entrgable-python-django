from django.shortcuts import redirect, render

# Create your views here.

from estudiantes.models import Estudiante
from django.views import View

class EstudiantesView(View):
    http_method_names = ['post', 'get']

    def get(self, request):
        estudiantes = Estudiante.objects.all()

        contexto = {
            "estudiantes": estudiantes
        }

        return render(request, 'estudiantes/lista_estudiantes.html', contexto)

    def post(self, request):
        Estudiante.objects.create(
            nombre = request.POST['nombre'],
            apellido = request.POST['apellido'],
            correo = request.POST['correo']
        )
        return self.get(request)

class EstudianteView(View):
    http_method_names = ['get']


    def get(self, request, id):
        try:
            estudiante = Estudiante.objects.get(id=id)
            clases = estudiante.clases.all()

            contexto = {
                'estudiante': estudiante,
                'clases': clases
            }

            return render(request, 'estudiantes/estudiante.html', contexto)
        except:
            return render(request, 'estudiantes/no_found.html', {'id': id})