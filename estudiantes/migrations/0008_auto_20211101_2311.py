# Generated by Django 2.2.24 on 2021-11-01 23:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('estudiantes', '0007_delete_estudiantesclase'),
    ]

    operations = [
        migrations.AlterField(
            model_name='estudiante',
            name='clases',
            field=models.ManyToManyField(blank=True, related_name='estudiantes', to='clases.Clase'),
        ),
    ]
